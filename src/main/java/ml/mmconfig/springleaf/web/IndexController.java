package ml.mmconfig.springleaf.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @Value("${ml.mmconfig.configValue:default}")
    private String configValue;

    @GetMapping(path = "/autoDeployCheck")
    public String autoDeployCheck() {
        return "autoDeployCheck OK";
    }

    @GetMapping(path = "/ip")
    public String getIp(ServerHttpRequest serverHttpRequest) {
        return serverHttpRequest.getRemoteAddress().getAddress().toString();
    }

    @GetMapping(path = "/configValue")
    public String getConfigValue() {
        return configValue;
    }

    @GetMapping(path = "/version")
    public String version() throws Exception {
        return "Version 0.3.0";
    }
}
