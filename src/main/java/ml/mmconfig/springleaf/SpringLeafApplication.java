package ml.mmconfig.springleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLeafApplication.class, args);
	}
}
